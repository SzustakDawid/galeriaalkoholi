app.controller('AlcoholController', function ($scope, $rootScope, Alcohols) {

    $scope.alcohols = {};


    $scope.isUserLogged = !!$rootScope.user;


    $scope.Koniaki = 0;
    $scope.Likiery = 0;
    $scope.Rumy = 0;
    $scope.Tequila = 0;
    $scope.Wszystkie = 0;

    $scope.selectedCategory = 5;

    $scope.selectedAlcohol;

    Alcohols.get(function (results) {
        results.forEach(function (el) {
            if (el.category == 1)
                $scope.Koniaki++;
            if (el.category == 2)
                $scope.Likiery++;
            if (el.category == 3)
                $scope.Rumy++;
            if (el.category == 4)
                $scope.Tequila++;
        })
        $scope.Wszystkie = $scope.Koniaki + $scope.Likiery + $scope.Rumy + $scope.Tequila;
        $scope.alcohols = results;
    });

    $scope.changeCategory = function (id) {
        $scope.selectedCategory = id;
    }

    $scope.displayRecord = function (id) {
        if (id == $scope.selectedCategory || $scope.selectedCategory == 5)
            return true;
        else
            return false;
    }

    $scope.getAlcoholDetail = function (alcohol) {
        $scope.selectedAlcohol = alcohol;
    }

    $scope.addToCart = function (alcohol) {
        var cart = angular.fromJson(localStorage.getItem("cart") || "{}");
        if (!cart[alcohol.id]) {
            cart[alcohol.id] = { alcohol: alcohol, quantity: 1 };
        } else {
            cart[alcohol.id].quantity += 1;
        }

        localStorage.setItem("cart", angular.toJson(cart));
        $rootScope.$broadcast("cartChanged");
    }

});