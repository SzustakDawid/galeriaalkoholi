app.controller('LoginController', function ($scope, $rootScope, $location) {

    $scope.register = function () {
        var users = angular.fromJson(localStorage.getItem("users") || "{}");

        if (!users[$scope.user.login]) {
            users[$scope.user.login] = {
                user: $scope.user
            };
            localStorage.setItem("users", angular.toJson(users));
            $scope.user = {};
            $scope.password = null;
            $scope.isError = false;
            $scope.isSuccess = true;
        }
        else {
            $scope.isError = true;
            $scope.isSuccess = false;
        }
    };

    $scope.logIn = function () {
        var users = angular.fromJson(localStorage.getItem("users") || "{}");

        if (!users[$scope.luser.login]) {
            $scope.isLoginError = true;
            return;
        }

        if ($scope.luser.password == users[$scope.luser.login].user.password) {
            $rootScope.user = users[$scope.luser.login].user;
            $scope.isLoginError = false;
            $rootScope.$broadcast("userLogIn");
            $location.url('/');
        }
        else
            $scope.isLoginError = true;
    };
});