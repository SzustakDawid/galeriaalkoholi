app.controller('NavbarController', function ($scope, $rootScope) {

    $scope.cartCount = getCartCount();

    $scope.$on('userLogIn', function () {
        $scope.isUserLogged = !!$rootScope.user;
    });

    $scope.$on('cartChanged', function () {
        $scope.cartCount = getCartCount();
    });

    $scope.logOut = function () {
        $rootScope.user = {};
        $scope.isUserLogged = false;
    }

    function getCartCount() {
        var cart = angular.fromJson(localStorage.getItem("cart") || "{}");
        var count = 0;
        for (var id in cart) {
            if (cart.hasOwnProperty(id)) {
                count += cart[id].quantity;
            }
        }
        return count;
    };

});
