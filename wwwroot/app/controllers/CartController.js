app.controller('CartController', function ($scope, $rootScope) {

    $scope.alcohols = angular.fromJson(localStorage.getItem("cart") || "{}");

    $scope.total = getTotal();
    $scope.shipCost = getShipCost();
    $scope.totalAndShipCost = getTotalAndShipCost();

    $scope.$on('cartChanged', function () {
        $scope.alcohols = angular.fromJson(localStorage.getItem("cart") || "{}");
        $scope.total = getTotal();
        $scope.shipCost = getShipCost();
        $scope.totalAndShipCost = getTotalAndShipCost();
    });

    $scope.remove = function (alcohol) {
        if (!!$scope.alcohols[alcohol.id]) {
            if ($scope.alcohols[alcohol.id].quantity == 1)
                delete $scope.alcohols[alcohol.id];
            else
                $scope.alcohols[alcohol.id].quantity--;
        }

        localStorage.setItem("cart", angular.toJson($scope.alcohols));
        $rootScope.$broadcast("cartChanged");
    }

    $scope.confirm = function () {
        localStorage.setItem("cart", angular.toJson({}));
        $rootScope.$broadcast("cartChanged");
    }

    function getTotal() {
        var total = 0;
        for (var item in $scope.alcohols) {
            total += $scope.alcohols[item].alcohol.price * $scope.alcohols[item].quantity;
        }
        return Math.round(total * 100) / 100
    }

    function getShipCost() {
        var cart = angular.fromJson(localStorage.getItem("cart") || "{}");
        var count = 0;
        for (var id in cart) {
            if (cart.hasOwnProperty(id)) {
                count += cart[id].quantity;
            }
        }
        return count * 10;
    };

    function getTotalAndShipCost() {
        return Math.round(getTotal() + getShipCost() * 100) / 100;;
    };

});
