'use stinct'
var app = angular.module('shopApp', ['ngRoute', 'ngResource']);

app.config(function ($routeProvider) {
    $routeProvider

        .when('/Contact', {
            templateUrl: 'app/pages/contact.html',
            controller: 'ContactController'
        })

        .when('/Alcohol', {
            templateUrl: 'app/pages/alcohol.html',
            controller: 'AlcoholController'
        })

        .when('/Login', {
            templateUrl: 'app/pages/login.html',
            controller: 'LoginController'
        })

        .when('/Profile', {
            templateUrl: 'app/pages/profile.html',
            controller: 'ProfileController'
        })

        .when('/Cart', {
            templateUrl: 'app/pages/cart.html',
            controller: 'CartController'
        })

        .when('/Confirm', {
            templateUrl: 'app/pages/confirm.html',
            controller: 'CartController'
        })

        .when('/Success', {
            templateUrl: 'app/pages/success.html',
            controller: 'CartController'
        })

        .otherwise({
            redirectTo: '/',
            templateUrl: 'app/pages/home.html',
            controller: 'HomeController'
        });
});

