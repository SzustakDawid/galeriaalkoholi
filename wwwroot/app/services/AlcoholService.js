app.factory("Alcohols", function ($resource) {
    return $resource('/content/data/alcohols.json', {}, {
        get: { method: 'GET', isArray: true }
    });
});